/**
 * Created by McPander on 18.10.2014.
 */

splevelApp.controller('userCtrl',['$window','$rootScope','$scope','$state', '$stateParams','Users','login','toaster',function($window,$rootScope,$scope,$state,$stateParams,Users,login,toaster) {

    console.log($stateParams);
    $scope.edit = false;
    var fields = [
        'firstName','lastName','organisation','email','login'
    ];
    $rootScope.$watch('authorized',function(newVal,oldVal){
        $scope.user = $rootScope.authorized;
        if($scope.user && $scope.user.password)
            delete $scope.user.password;
    });
    var validation = function(values){
        if(values.pass1 !== values.pass2){
            return false;
        }else if(values.pass1.length < 5){
            return false;
        }
        return true;
    };
    $scope.editUser = function(user){
        if(user.password)
            delete user.password;
        Users.edit(user,function(success){
            toaster.pop('success', "Успешно", "");
            console.log(success);
        },function(error){
            var errors = error.data.errors;
            var toaster_str = '';
            for(var q in errors){
                if(toaster_str != '')
                    toaster_str += '\n';
                toaster_str += errors[q];
            }
            toaster.pop('error', "Ошибка", toaster_str);
        })
    };
    $scope.checkLogin = function(user){
        login.check({login:user},function(success){
            if(success["Статус"] == "Занят"){
                $scope.backend_errors['login'] = success["Статус"];
                //$scope.registration.submitted = true;
                $scope.registration.login = {
                    $invalid:true,
                    $dirty:true
                };
                $scope.loginErr = true;
            }else{
                $scope.registration.login = {
                    $invalid:false,
                    $dirty:false
                };
                $scope.loginErr = false;
            }
        },function(error){
            console.log(error);
        });
    };
    $scope.changePassword = function(user){
        $scope.backend_errors = {};
        $scope.submitted = false;
        var passwords = {
            pass1:user.password,
            pass2:user.passwordrepeat
        };
        var valid = validation(passwords);
        if(valid){
            Users.edit(user,
                function(success){
                    toaster.pop('success', "Успешно", "");
                    console.log(success);
                },function(error){
                    $scope.submitted = true;
                    var errors = error.data.errors;
                    var toaster_str = '';
                    for(var q in errors){
                        if(_.indexOf(fields,q) != -1){
                            $scope.backend_errors[q] = errors[q];
                        }else{
                            if(toaster_str != '')
                                toaster_str += '\n';
                            toaster_str += errors[q];
                        }
                    }
                    toaster.pop('error', "Ошибка", toaster_str);
                })
        }
    };

    console.log('user');
}]);