/**
 * Created by McPander on 18.10.2014.
 */
var splevelApp = angular.module('splevelApp',
    [
        'ui.router',
        'ngResource',
        'angular-loading-bar',
        'ngAnimate',
        'toaster',
        'ja.qr'
    ]);
splevelApp.constant("apiUrl", "localhost:8080");
splevelApp.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state('index',{
            url: '/',
            templateUrl: 'modules/index/templates/index.html',
            controller: 'splCtrl'
        }).state('user',{
            url: '/user',
            templateUrl: 'modules/user/templates/user.html',
            controller: 'userCtrl'
        }).state('other',{
            url: '/other',
            templateUrl: 'modules/index/templates/index.html',
            controller: function($scope){
                console.log('other');
            }
        }).state('report',{
            url: '/report/:id',
            templateUrl: 'modules/report/templates/report.html',
            controller: 'reportCtrl'
        });
});
