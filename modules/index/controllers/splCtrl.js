/**
 * Created by McPander on 18.10.2014.
 */

splevelApp.controller('splCtrl',['$window','$rootScope','$scope','$state', '$stateParams','location','splModel','splModelHal','noiseLevel','report','toaster',function($window,$rootScope,$scope,$state,$stateParams,location,splModel,splModelHal,noiseLevel,report,toaster) {


    $scope.redirect = function(){
        console.log(1);
        $state.transitionTo($state.current, $stateParams, {
            reload: true,
            inherit: false,
            notify: true
        });
    };

    $scope.role = 'default';
    $rootScope.$watch('authorized',function(newVal,oldVal){
        if(newVal !== oldVal){
            $scope.authorized = $rootScope.authorized;
        }
    });
    $scope.ready = false;
    var pointOptions = {
        preset: 'islands#circleIcon',
        iconColor: '#0095b6'
    };
    var polygonOptions = {
        // Описываем опции геообъекта.
        // Цвет заливки.
        fillColor: '#00FF00',
        // Цвет обводки.
        strokeColor: '#00EE00',
        // Общая прозрачность (как для заливки, так и для обводки).
        opacity: 0.5,
        // Ширина обводки.
        strokeWidth: 5
    };
    var ymaps;
    var layer;
    var splmap;
    var presets = {
        center:[59.92977684837557, 30.368797],
        zoom:13
    };
    var czoom = presets.zoom;
    var geoPoints = [];
    var geoPolygons = {};
    var hiddenPoints = [];
    var shownPolygons = [];
    var max = 5;
    var clusterer;
    var storage = {};
    $scope.limit = 5;
    $scope.test = false;
    $scope.center = false;

    $window.ymaps.ready(function(map){
        ymaps = map;
        layer = new ymaps.Layer(function(tile,zoom){
            return 'styles/img/copylayer.png';
        }, {
            tileTransparent: true
        });
        splmap = new ymaps.Map("spl-map", {
            center: presets.center,
            zoom: presets.zoom,
            controls:['zoomControl','rulerControl','searchControl']
        });
        splmap.layers.add(layer);
        splmap.controls.add(new ymaps.control.TypeSelector(['yandex#map', 'yandex#publicMap']));
        clusterer = new ymaps.Clusterer({
            maxZoom:15
        });

        splmap.copyrights.add('&copy;infostrata.ru');

        splmap.events.add('actionend', function(e){
            var target = e.get('target');
            var action = e.get('action');
            var zoom = target.getZoom();
            if(czoom > 15 && zoom <= 15){
                for(var i = 0;i<shownPolygons.length;i++){
                    splmap.geoObjects.remove(shownPolygons[i]);
                }
                hiddenPoints = [];
                shownPolygons = [];
                storage = {};
                czoom = zoom;
                return false;
            }
            czoom = zoom;
            if(!_.isEmpty(storage)){
                var buff = ymaps.geoQuery(geoPoints).searchIntersect(splmap);
                if(_.isEqual(storage,buff)){
                    return false;
                };
            }
            if(shownPolygons.length > 0){
                for(var i = 0;i<shownPolygons.length;i++){
                    splmap.geoObjects.remove(shownPolygons[i]);
                }
                hiddenPoints = [];
                shownPolygons = [];
            }
            if(zoom > 15){
                storage = ymaps.geoQuery(geoPoints).searchIntersect(splmap);
                storage.each(function(el,i){
                    var gPolygon = geoPolygons[el.properties.get('balloonContent')];
                    shownPolygons.push(gPolygon);
                    splmap.geoObjects.add(gPolygon);
                });
            }
        });

        splMapInit();
    });
    function getCenter(){
        location.read(function(success){
            $scope.center = success;
        },function(error){
            $scope.center = false;
        });
    }

    var plugins = {};
    function pluginConstructor(user){
        console.log(user);
        $scope.about = [];
        for(var q in plugins){
            plugins[q].remove();
            delete plugins[q];
        }
        if(user){
            $scope.role = user.roles[0].roleName.toLowerCase();
            console.log($scope.role);
            plugins['map-settings'] = new McPopup({
                position:{left:'20px',top:'100px'},
                title:'',
                header:false,
                drugble:false,
                content:{
                    templateUrl:'components/'+$scope.role+'/settings/template/settings.html'
                },
                eventsObject:{
                    url:'components/'+$scope.role+'/settings/src/script.js',
                    scope:$scope
                }
            });
            plugins['map-editor'] = new McPopup({
                position:{left:'20px',top:'150px'},
                title:'',
                header:false,
                drugble:false,
                content:{
                    templateUrl:'components/'+$scope.role+'/mapeditor/template/mapeditor.html'
                },
                eventsObject:{
                    url:'components/'+$scope.role+'/mapeditor/src/script.js',
                    scope:$scope,
                    data:{
                        constructor:ymaps,
                        map:splmap
                    }
                }
            });
            splmap.layers.remove(layer);
        }else{
            $scope.role = 'default';
            splmap.layers.add(layer);
        }
    }

    function splMapInit(){
        getCenter();
        readList({});
        if(!$scope.init){
            $scope.init = true;
            $scope.$watch('authorized',function(newVal,oldVal){
                pluginConstructor(newVal);
            });
            $scope.$watch('center',function(nVal,oVal){
                if(nVal !== oVal){
                    if(nVal){
                        presets.center = nVal;
                        splmap.setCenter(presets.center);
                    }
                }
            });
            $scope.$watch('polygons',function(newValue,oldValue){
                if(newValue !== oldValue){
                    if(newValue){
                        clusterer.removeAll();
                        splmap.geoObjects.removeAll();
                        //splmap.setZoom(presets.zoom);
                        //splmap.setCenter(presets.center);
                        geoPoints = [];
                        geoPolygons = [];
                        hiddenPoints = [];
                        shownPolygons = [];
                        for(var i = 0;i<newValue.length;i++){
                            var splObject = makePolygonObject(newValue[i]);
                            var splObjectPoint = makePointObject(newValue[i]);
                            var geoSPLObjectPoint = new ymaps.GeoObject(splObjectPoint.geometry,pointOptions);
                            geoSPLObjectPoint.events.add('click',function(e){
                                var target = e.get('target');
                                splmap.setCenter(target.geometry.getCoordinates());
                                splmap.setZoom(17);
                            });
                            geoPoints.push(geoSPLObjectPoint);
                            geoPolygons[splObject.id] = new ymaps.GeoObject(splObject.geometry,polygonOptions);
                            geoPolygons[splObject.id].events.add('click',function(e){
                                var target = e.get('target');
                                var id = target.properties.get('balloonContent');
                                var added = _.findIndex($scope.about,function(item){ return item.soundAreaId == id});
                                if($scope.about.length < max && added == -1){
                                    var params = {id:id,action:'info'};
                                    if($scope.hasActualComment && $scope.hasActualComment === true){
                                        params['isFullProjection']=true;
                                    }
                                    splModel.info(
                                        params,
                                        function(data){
                                            $scope.about.push(data);
                                            plugins[id] = new McPopup({
                                                top:60,
                                                drugble:true,
                                                header:true,
                                                buttons:true,
                                                title:data.name+' #'+data.soundAreaId,
                                                content:{
                                                    templateUrl:'components/'+$scope.role+'/speakerinfo/template/speakerinfo.html',
                                                    values:data
                                                },
                                                eventsObject:{
                                                    url:'components/'+$scope.role+'/speakerinfo/src/script.js',
                                                    scope:$scope,
                                                    data:data
                                                }

                                            });
                                        },
                                        function(error){console.log(error);}
                                    );
                                };
                            });
                        }
                        clusterer.add(geoPoints);
                        splmap.geoObjects.add(clusterer);
                    }else{
                        console.log('ERROR:no data');
                    }
                }
            });
        }
    }

    $scope.getNoiseLevel = function(object){
        noiseLevel.read(object,function(data){
            $scope.noiseLevel = data;
        })
    };
    $scope.getCount = function(){
        splModel.count({},function(data){
            $scope.count = data;
        })
    };
    $scope.getReport = function(splzone){
        report.createReport({speakerLocationId:splzone.speakerLocationId},function(success){
            $state.go('report',{id:success.id});
        },function(error){
            console.log(error);
        });
    };
    $scope.removeSPLZ = function(splZone){
        splModelHal.deleteSpeakerLocation({id:splZone.speakerLocationId},function(success){
            readList({});
            $scope.removeSPLZInfo(splZone);
            toaster.pop('success','Удалено');
        },function(error){
            toaster.pop('error','Ошибка');
        });
    };
    $scope.getSpeakersList = function(object){
        splModel.readNew(object,function(data){
            $scope.speakers = data;
        },function(error){
            console.log(error);
        })
    };
    $scope.getSpeakers = function(){
        splModelHal.speakers({},function(success){
            console.log(success);
            $scope.fullSpeakersList = success;
        },function(error){
            console.log(error);
        });
    };
    $scope.getSpeakerZone = function(object){
        splModel.calculateNew(object,function(success){
            $scope.calculatedZone = success[0];
            console.log(success);
        },function(error){
            console.log(error);
        });
    };
    $scope.sendSPLZError = function(errorObject){
        console.log(errorObject);
        splModelHal.error(errorObject,function(success){
            toaster.pop('success',"Успешно",'');
        },function(error){
            toaster.pop('error',"Ошибка",'');
        });
    };

    var pressed = false;
    $scope.createNewSPLZone = function(object,callback){
        if(pressed)
            return false;
        else
            pressed = true;
        splModel.createNew(object,function(success){
            toaster.pop('success',"Успешно",'');
            callback();
            setTimeout(function(){
                pressed = false;
            },500);
        },function(error){
            setTimeout(function(){
                pressed = false;
            },500);
            toaster.pop('error',"Ошибка",'');
            console.log(error);
        });
    };

    $scope.confirmZone = function(zone){
        splModel.confirm({id:zone.id},{confirmed:true},function(success){
            toaster.pop('success',"Успешно",'');
            readList({});
            console.log(success);
        },function(error){
            toaster.pop('error',"Ошибка",'');
            console.log(error);
        });
    };

    $scope.confirmComment = function(zone){
        splModelHal.speakerCommentApprove(zone,function(success){
            toaster.pop('success',"Успешно",'');
            readList({});
            console.log(success);
        },function(error){
            toaster.pop('error',"Ошибка",'');
            console.log(error);
        });
    };
    $scope.removeComment = function(zone){
        splModelHal.speakerCommentDelete(zone,function(success){
            toaster.pop('success',"Успешно",'');
            readList({});
            console.log(success);
        },function(error){
            toaster.pop('error',"Ошибка",'');
            console.log(error);
        });
    };

    $scope.setAdminFilter = function(){
        readList({});
    };
    $scope.setTest = function(test){
        $scope.test = test;
        readList({});
    };
    $scope.setLimit = function(limit){
        if(_.isNumber(parseFloat(limit))){
            $scope.limit = limit;
            readList({});
        }
    };
    $scope.setMapLayer = function(){
        splmap.layers.add(new ymaps.Layer(
            function(tilenumber,zoom){
                var url = 'http://c.tile.openstreetmap.org/${z}/${x}/${y}.png';
                url = url.replace('${z}',zoom);
                url = url.replace('${x}',tilenumber[0]);
                url = url.replace('${y}',tilenumber[1]);
                return url;
            },
            {}
        ));
    };

    $scope.about = [];
    $scope.removeSPLZInfo = function(splZone){
        if(plugins[splZone.soundAreaId]){
            plugins[splZone.soundAreaId].remove();
            delete plugins[splZone.soundAreaId];
        }
        _.remove($scope.about,function(item){
            return item === splZone;
        });
    };

    $scope.isConfirmed = true;
    $scope.hasActualComment = false;

    function readList(params){
        if($scope.isConfirmed !== undefined && !$scope.isConfirmed){
            params.isConfirmed = $scope.isConfirmed;
        }
        if($scope.hasActualComment !== undefined && $scope.hasActualComment){
            params.hasActualComment = $scope.hasActualComment;
        }
        params.limit = $scope.limit;
        params.test = $scope.test;
        splModel.read(
            params,
            function(data){
                $scope.polygons = data;
            },
            function(error){
                $scope.polygons = false;
            }
        );
    }
    function makePolygonCoords(coordinates){
        var arr = [];
        if(coordinates && coordinates.length>0){
            for(var i = 0;i<coordinates.length;i++){
                arr.push([coordinates[i].latitude,coordinates[i].longitude]);
            }
        }
        return arr;
    }
    function makePolygonObject(polygon){
        var object = false;
        if(polygon && polygon.id){
            object = {
                id:polygon.id
            };
            var coordinates = polygon.coordinates;
            object.geometry = {
                geometry:{
                    type:"Polygon",
                    coordinates:[coordinates,[]]
                },
                properties:{
                    balloonContent:polygon.id
                }
            }
        }
        return object;
    };
    function makePointObject(polygon){
        var object = false;
        if(polygon && polygon.id){
            object = {
                id:polygon.id
            };
            var center = polygon.speakerLocationCoordinate;
            object.geometry = {
                geometry:{
                    type:"Point",
                    coordinates:center
                },
                properties:{
                    balloonContent:polygon.id
                }
            }
        }
        return object;
    };

}]);