/**
 * Created by McPander on 23.11.2014.
 */
splevelApp.controller('menuCtrl',['$window','$rootScope','$scope','$state', '$stateParams','authorize','authorized','toaster',function($window,$rootScope,$scope,$state,$stateParams,authorize,authorized,toaster) {
    $rootScope.authorized = false;
    authorized.check({},function(success){
        $rootScope.authorized = success;
    },function(error){
        console.log(error);
    });
    $scope.authorize = function(login){
        if($rootScope.authorized){
            authorize.logout(function(success){
                //console.log(success);
            },function(error){
                //console.log(error);
            });
            $rootScope.authorized = false;
            return false;
        }else if(login && login.login && login.login.length > 3 && login.password && login.password.length > 3){
            //console.log(login);
            authorize.login(login,function(success){
                //console.log(success);
                $scope.login = {
                    login:'',
                    password:''
                };
                $rootScope.authorized = success;
            },function(error){
                var errors = error.data.errors;
                var toaster_str = '';
                for(var q in errors){
                    if(toaster_str != '')
                        toaster_str += '\n';
                    toaster_str += errors[q];
                }
                console.log(toaster_str);
                toaster.pop('error', "Ошибка", toaster_str);
            });
        }
        //$rootScope.$broadcast('spl-authorize', $rootScope.authorized);
    };
}]);