/**
 * Created by McPander on 18.10.2014.
 */

splevelApp.factory('registration',['$resource','apiUrl',function($resource,apiUrl) {

    var registration = $resource('http://'+apiUrl+'/api/user',
        null,
        {
            registration: {method: 'POST', params:{}}
        }
    );

    return registration;
}]).factory('login',['$resource','apiUrl',function($resource,apiUrl) {

    var login = $resource('http://'+apiUrl+'/api/login',
        null,
        {
            check: {method: 'GET', params:{}}
        }
    );

    return login;
}]);