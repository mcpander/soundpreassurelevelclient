/**
 * Created by McPander on 18.10.2014.
 */
splevelApp.factory('splModel',['$resource','apiUrl',function($resource,apiUrl) {

    var spModel = $resource('http://'+apiUrl+'/api/soundArea/:id/:action',
        null,
        {
            read: {method: 'GET', params:{limit:10}, headers:{},isArray:true},
            info: {method: 'GET', params:{id:'@id',action:'info'}, headers:{}},
            readNew: {method: 'GET', params:{action:'calculate'}, headers:{},isArray:true},
            createNew: {method: 'POST', params:{}, headers:{}},
            calculateNew: {method: 'GET', params:{action:'calculate'}, headers:{},isArray:true},
            confirm: {method: 'PATCH', params:{id:'@id'}, headers:{}},
            count:{method: 'GET', params:{action:'count'}, headers:{}}
        }
    );

    return spModel;
}]).factory('splModelHal',['$resource','apiUrl',function($resource,apiUrl) {

    var splModelHal = $resource('http://'+apiUrl+'/repository/:action/:id',
        null,
        {
            error: {method: 'POST', params:{action:'speakerLocationComment'}, headers:{}},
            speakers: {method: 'GET', params:{action:'hornSpeaker',size:100}, headers:{}},
            deleteSpeakerLocation: {method: 'DELETE', params:{action:'speakerLocation',id:'@id' }, headers:{}},
            speakerCommentApprove: {method: 'PUT', params:{action:'speakerLocationComment',id:'@id'}, headers:{}},
            speakerCommentDelete: {method: 'DELETE', params:{action:'speakerLocationComment',id:'@id'}, headers:{}},
            getReport: {method: 'GET', params:{action:'report',id:'@id'}, headers:{}},
            getReports: {method: 'GET', params:{action:'report',size:100}, headers:{}}
        }
    );

    return splModelHal;
}]).factory('noiseLevel',['$resource','apiUrl',function($resource,apiUrl) {

    var noiseLevel = $resource('http://'+apiUrl+'/api/noise/:action',
        null,
        {
            read: {method: 'GET', params:{action:'search'}, headers:{}}
        }
    );

    return noiseLevel;
}]).factory('report',['$resource','apiUrl',function($resource,apiUrl) {

    var report = $resource('http://'+apiUrl+'/api/report/:action',
        null,
        {
            createReport: {method: 'POST', params:{}, headers:{}}
        }
    );

    return report;
}]);