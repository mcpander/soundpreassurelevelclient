/**
 * Created by McPander on 18.10.2014.
 */

splevelApp.factory('Users',['$resource','apiUrl',function($resource,apiUrl) {
    var Users = $resource('http://'+apiUrl+'/api/user/:id',
        null,
        {
            edit: {method: 'PUT', params:{id:'@id'}, headers:{}}
        }
    );
    return Users;
}]);