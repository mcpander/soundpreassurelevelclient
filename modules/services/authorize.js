/**
 * Created by McPander on 18.10.2014.
 */

splevelApp.factory('authorize',['$resource','apiUrl',function($resource,apiUrl) {
    var authorize = $resource('http://'+apiUrl+'/api/login',
        null,
        {
            login: {method: 'POST', params:{}, headers:{}},
            logout: {method: 'DELETE', params:{}, headers:{}}
        }
    );
    return authorize;
}]).factory('authorized',['$resource','apiUrl',function($resource,apiUrl) {
    var authorized = $resource('http://'+apiUrl+'/api/loggedin',
        null,
        {
            check: {method: 'GET', params:{}, headers:{}}
        }
    );
    return authorized;
}]);