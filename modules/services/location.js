/**
 * Created by McPander on 18.10.2014.
 */

splevelApp.factory('location',['$resource','apiUrl',function($resource,apiUrl) {

    var location = $resource('http://'+apiUrl+'/api/location',
        null,
        {
            read: {method: 'GET', params:{}, headers:{},isArray:true}
        }
    );

    return location;
}]);