/**
 * Created by McPander on 18.10.2014.
 */

splevelApp.controller('reportCtrl',['$window','$rootScope','$scope','$state', '$stateParams','$parse','location','splModelHal','toaster',function($window,$rootScope,$scope,$state,$stateParams,$parse,location,splModelHal,toaster) {
    var ymaps,splmap;
    $scope.qrCodeString = '';
    $scope.size = 150;
    $scope.correctionLevel = '';
    $scope.typeNumber = 0;
    $scope.inputMode = '';
    $scope.image = true;
    var presets = {
        center:[59.92977684837557, 30.368797],
        zoom:16
    };
    var polygonOptions = {
        fillColor: '#00FF00',
        strokeColor: '#00EE00',
        opacity: 0.5,
        strokeWidth: 5
    };
function makeReport(){
    if($stateParams && $stateParams.id && $scope.authorized){
        splModelHal.getReport({id:$stateParams.id},function(success){
            $scope.report = success;
            $scope.qrCodeString = 'infostrata.ru/#/report/'+$stateParams.id;
        },function(error){
            console.log(error);
        });
    };
};
    $window.ymaps.ready(function(map) {
        ymaps = map;
        var layer = new ymaps.Layer(function(tile,zoom){
            return 'styles/img/copylayer.png';
        }, {
            tileTransparent: true
        });
        splmap = new ymaps.Map("map", {
            center:presets.center,
            zoom:presets.zoom,
            controls: []
        });
        splmap.copyrights.add('&copy;infostrata.ru');
        splmap.behaviors.disable(['drag','rightMouseButtonMagnifier','leftMouseButtonMagnifier','ruler','DblClickZoom','MultiTouch','RouteEditor','scrollZoom']);
        splmap.layers.add(layer);
        makeReport();
    });


    $rootScope.$watch('authorized',function(newVal,oldVal){
        if(newVal !== oldVal){
            $scope.authorized = $rootScope.authorized;
        }
    });

    $scope.$watch('report',function(nVal,oVal){
        if(nVal !== oVal){
            console.log(nVal);
            splmap.setCenter($scope.$eval(nVal.speakerLocationCoordinates),presets.zoom);
            var circleLayout = ymaps.templateLayoutFactory.createClass('<div class="placemark_layout_container"><div class="circle_layout">1</div></div>');
            var circlePlacemark = new ymaps.Placemark(
                $scope.$eval(nVal.speakerLocationCoordinates), {
                }, {
                    iconLayout: circleLayout,
                    iconShape: {
                        type: 'Circle',
                        coordinates: [0, 0],
                        radius: 10
                    }
                }
            );
            splmap.geoObjects.add(circlePlacemark);
            //
            var polygon = makePolygonObject(nVal.id,$scope.$eval(nVal.soundAreaCoordinates));
            console.log(polygon);
            splmap.geoObjects.add(new ymaps.GeoObject(polygon.geometry,polygonOptions));

        }
    });


    function makePointObject(polygon){
        var object = false;
        if(polygon && polygon.id){
            object = {
                id:polygon.id
            };
            var center = polygon.speakerLocationCoordinate;
            object.geometry = {
                geometry:{
                    type:"Point",
                    coordinates:center
                },
                properties:{
                    balloonContent:polygon.id
                }
            }
        }
        return object;
    };
    function makePolygonObject(id,polygon){
        console.log(polygon)
        var object = false;
        if(polygon && id){
            object = {
                id:id
            };
            var coordinates = polygon;
            object.geometry = {
                geometry:{
                    type:"Polygon",
                    coordinates:[coordinates,[]]
                }
            }
        }
        return object;
    };


}]);