/**
 * Created by McPander on 18.10.2014.
 */

splevelApp.controller('registrationCtrl',['$window','$scope','$state', '$stateParams','registration','login','toaster',function($window,$scope,$state,$stateParams,registration,login,toaster) {

    console.log($stateParams);
    $scope.backend_errors = {};
    var fields = [
        'firstName','lastName','organisation','email','login','password'
    ];

    var validation = function(values){
        if(values.pass1 !== values.pass2){
            return false;
        }else if(values.pass1.length < 5){
            return false;
        }
        return true;
    };
    $scope.checkLogin = function(user){
        login.check({login:user},function(success){
            console.log(success["Статус"]);
            if(success["Статус"] == "Занят"){
                $scope.backend_errors['login'] = success["Статус"];
                //$scope.registration.submitted = true;
                $scope.registration.login = {
                    $invalid:true,
                    $dirty:true
                };
                $scope.loginErr = true;
            }else{
                $scope.registration.login = {
                    $invalid:false,
                    $dirty:false
                };
                $scope.loginErr = false;
            }
        },function(error){
            console.log(error);
        });
    };
    $scope.registr = function(user){
        $scope.backend_errors = {};
        $scope.registration.submitted = false;
        var passwords = {
            pass1:user.password,
            pass2:user.passwordrepeat
        };
        var valid = validation(passwords);
        if(valid){
            registration.registration(user,
            function(success){
                toaster.pop('success', "Успешно", "");
                var modal = $('#myModal');
                modal.modal('show');
                console.log(success);
            },function(error){
                $scope.registration.submitted = true;
                var errors = error.data.errors;
                var toaster_str = '';
                for(var q in errors){
                    if(_.indexOf(fields,q) != -1){
                        $scope.backend_errors[q] = errors[q];
                    }else{
                        if(toaster_str != '')
                            toaster_str += '\n';
                        toaster_str += errors[q];
                    }
                }
                toaster.pop('error', "Ошибка", toaster_str);
            })
        }
    };

}]);