/**
 * Created by McPander on 02.11.2014.
 */


var McPopup = function(properties){
    this.drugble = properties.drugble;
    this.buttons = properties.buttons;
    this.header = properties.header;

//    this.classes = properties.position || [];
    this.classes = properties.classes || [];
    this.mintop = properties.top || 10;
    this.position = properties.position || false;
    this.title = properties.title || 'McPopup';
    this.footer = properties.footer || false;
    this.content = properties.content || 'McPopup Content';
    this.events = properties.events || false;
    this.eventsObject = properties.eventsObject || false;
    this.mccontainer = document.createElement('div');

    this.getTemplate = function(url){
        var result = false;
        $.ajax({
            type: "GET",
            url: url,
            async:false,
            success: function(msg){
                result = msg;
            }
        });
        return result;
    };
    this.getEvents = function(eventsObject){
        var self = this;
        var result = false;
        $.ajax({
            url: eventsObject.url,
            dataType: "script",
            async:false,
            success: function(data){
                result = events(eventsObject.scope,eventsObject.data,self.mccontainer);
            }
        });
        return result;
    };
    this.makeTemplate = function(template){
        var str = template;
        str = str.replace('{{title}}',this.title);
        str = str.replace('{{footer}}',this.footer);
        if(this.content instanceof String){
            str = str.replace('{{content}}',this.content);
        }else if(this.content instanceof Object){
            var tpl = '';
            if(this.content.template){
                tpl = this.content.template;
            }else if(this.content.templateUrl){
                tpl = this.getTemplate(this.content.templateUrl);
            }
            if(this.content.values){
                for(var q in this.content.values){
                    tpl = tpl.replace('{{'+q+'}}',this.content.values[q]);
                }
            }
            str = str.replace('{{content}}',tpl);
        }
        return str;
    };
    this.findPosition = function(){
        if(this.position){
            for(var q in this.position){
                console.log(q)
                this.mccontainer.style[q] = this.position[q];
            }
        }else{
            var popups = document.querySelectorAll('.mcpopup');
            var maxtop = 0;
            if(popups && popups.length > 0){
                for(var i = 0;i < popups.length;i++){
                    var bcr = popups[i].getBoundingClientRect();
                    if(bcr.top > maxtop){
                        maxtop = bcr.top;
                    }
                }
            }
            this.mccontainer.style.top = (maxtop+this.mintop)+'px';
            this.mccontainer.style.right = '10px';
        }
    };
    this.makeMcPopupContainer = function(){
        this.mccontainer.setAttribute('class','mcpopup');
        var self = this.mccontainer;
        var drugen = false;
        var coords = [];
        this.findPosition();
        var mainTemplate = this.makeTemplate(this.getTemplate('plugins//mcpopup/templates/mcpopup.html'));
        this.mccontainer.innerHTML = mainTemplate;
        if(this.buttons){
            $(this.mccontainer.querySelector('.button-close')).bind('click',function(){
                self.parentNode.removeChild(self);
            });
            $(this.mccontainer.querySelector('.button-min')).bind('click',function(){
                if(!$(self).find('.mcpopup-body').hasClass('hide')){
                    $(self).find('.mcpopup-body').addClass('hide');
                }
            });
            $(this.mccontainer.querySelector('.button-max')).bind('click',function(){
                if($(self).find('.mcpopup-body').hasClass('hide')){
                    $(self).find('.mcpopup-body').removeClass('hide');
                }
            });
        }else{
            $(this.mccontainer.querySelector('.header-buttons')).addClass('hide');
        }
        if(!this.header){
            $(this.mccontainer.querySelector('.mcpopup-header')).addClass('hide');
        }
        if(!this.footer){
            $(this.mccontainer.querySelector('.mcpopup-footer')).addClass('hide');
        }
        if(this.drugble){
            $(this.mccontainer).bind('click',function(event){
                event.cancelBubble = false;
                event.stopPropagation();
            });
            $(this.mccontainer).bind('mousedown',function(event){
                coords = [event.pageX,event.pageY];
                drugen = true;
            });
            $(document).bind('mouseup',function(event){
                drugen = false;
            });
            $(document).bind('mousemove',function(event){
                if(drugen){
                    var delta = [coords[0]-event.pageX,coords[1]-event.pageY];
                    coords = [event.pageX,event.pageY];
                    if(self.style.right && self.style.top){
                        self.style.right = (parseFloat(self.style.right) + delta[0])+'px';
                        self.style.top = (parseFloat(self.style.top) - delta[1])+'px';
                    }else{
                        self.style.right = (delta[0])+'px';
                        self.style.top = (delta[1])+'px';
                    }
                }
            });
        }
    };
    this.initEvents = function(){
        if(this.eventsObject){
            this.events = this.getEvents(this.eventsObject);
        }
        if(this.events){
            for(var q in this.events){
                var arr = q.split(' ');
                if(arr.length == 2){
                    $(this.mccontainer).find(arr[0]).bind(arr[1],this.events[q]);
                }
            }
        }
    };
    this.init = function(){
        this.makeMcPopupContainer();
        this.initEvents();
        document.querySelector('body .spl-app-container').appendChild(this.mccontainer);
    };

    this.remove = function(){
        //if(this.mccontainer)
        //    document.querySelector('body .spl-app-container').removeChild(this.mccontainer);
        if(this.mccontainer && this.mccontainer.parentNode)
            this.mccontainer.parentNode.removeChild(this.mccontainer);
    };

    this.init();
};
