/**
 * Created by McPander on 02.11.2014.
 */
var scripts = ['plugins/mcpopup/bower_components/jquery/jquery.min.js','plugins/mcpopup/js/main.js'];
var styles = ['plugins/mcpopup/bower_components/font-awesome/css/font-awesome.min.css','plugins/mcpopup/css/style.css'];
var header = document.querySelector('head');

for(var i = 0;i<styles.length;i++){
    var style = document.createElement('link');
    style.setAttribute("rel","stylesheet");
    style.setAttribute("href", styles[i]);
    header.appendChild(style);
}
for(var i = 0;i<scripts.length;i++){
    var script = document.createElement('script');
    script.setAttribute("type","text/javascript");
    script.setAttribute("src", scripts[i]);
    header.appendChild(script);
}