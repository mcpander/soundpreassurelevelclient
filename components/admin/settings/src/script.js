/**
 * Created by McPander on 29.11.2014.
 */

var events = function(scope,data,container){
    console.log(1);
    var countMod = $($(container).find('.count-moder'));
    var countComment = $($(container).find('.count-comment'));
    scope.$watch('count',function(nVal,oVal){
        if(nVal !== oVal){
            var conf = '('+nVal.isConfirmed+'/'+nVal.isNotConfirmed+')';
            var comm = '('+nVal.isConfirmedAndHasActualComment+')';
            countMod.html(conf);
            countComment.html(comm);
        }
    });
    return {
        '.setting-button click':function(event){
            var settings = $(this.parentNode.querySelector('.settings'));
            if(settings.hasClass('hide')){
                scope.getCount();
                settings.removeClass('hide');
            }
            else
                settings.addClass('hide');
        },
        '.test change':function(event){
            var el = this;
            while(!$(el).hasClass('mcpopup')){
                el = el.parentNode;
            }
            el.querySelector('.quantity').disabled = !this.checked;
            scope.setTest(this.checked);
        },
        '.actual change':function(event){
            scope.isConfirmed = this.checked;
            scope.setAdminFilter();
        },
        '.commented change':function(event){
            scope.hasActualComment = this.checked;
            scope.setAdminFilter();
        },
        '.quantity keyup':function(event){
            var value = this.value;
            setTimeout(function(){
                scope.setLimit(value);
            },100)
        }
    };
};