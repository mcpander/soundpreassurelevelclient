/**
 * Created by McPander on 29.11.2014.
 */
var events = function(scope,data,container){
    var action = $(container).find('.admin-action');
    console.log(data);
    //data.hasActualComment = true;
    if(data.confirmed === false){
        action.removeClass('hide');
        action.find('.confirm').removeClass('hide');
    }
    if(data.comments && data.comments.length > 0){
        action.removeClass('hide');
        var comments = action.find('.comments');
        comments.removeClass('hide');
        for(var i = 0;i<data.comments.length;i++){
            if(data.comments[i].actual){
                comments.append('<div class="row"><div class="col-md-9 text-left">'+data.comments[i].comment+'</div><div class="col-md-1"><i id="'+i+'" class="fa fa-check save"></i></div><div class="col-md-1"><i class="fa fa-close cancel"></i></div></div>');
            }
        }
    }
    return {
        '.spl-info click':function(event){
            console.log('super',scope.about.length,data);
        },
        '.button-close click':function(event){
            console.log(scope.about,data);
            scope.removeSPLZInfo(data);
            stopEvent(event);
        },
        '.confirm click':function(event){
            var object = {
                id:data.soundAreaId,
                confirmed:true
            };
            scope.confirmZone(object);
        },
        '.make-report click':function(event){
            scope.getReport(data);
        },
        '.remove click':function(event){
            if(window.confirm('Подтвердите удаление!')){
                scope.removeSPLZ(data);
            }
        },
        '.save click':function(event){

            data.comments[this.id]['actual'] = false;
            data.comments[this.id]['speakerLocation']="/repository/speakerLocation/"+data.speakerLocationId;
            scope.confirmComment(data.comments[this.id]);
            scope.setAdminFilter();
        },
        '.cancel click':function(event){
            data.comments[this.id]['speakerLocation']="/repository/speakerLocation/"+data.speakerLocationId;
            scope.removeComment(data.comments[this.id]);
            scope.setAdminFilter();
        }
    };
};
function stopEvent(event){
    event.stopPropagation();
    event.cancelBubble = false;
}