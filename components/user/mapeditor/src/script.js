/**
 * Created by McPander on 29.11.2014.
 */

var events = function(scope,data,container){
    var centerx,centery,vectorx,vectory,vtype,height,noise,speakers;
    var editor = false;
    var lock = false,vlock = false;
    var edit = false;
    var ymaps = data.constructor;
    var map = data.map;
    var newPoint,vectorPoint,vectorLine,polygon = false;

    var h,n;

    var polygonOptions = {
        // Описываем опции геообъекта.
        // Цвет заливки.
        fillColor: '#0000FF',
        // Цвет обводки.
        strokeColor: '#00EE00',
        // Общая прозрачность (как для заливки, так и для обводки).
        opacity: 0.5,
        // Ширина обводки.
        strokeWidth: 5
    };
    vectorLine = new ymaps.GeoObject({
        geometry: {
            type: "LineString"
        }
    });

    var editLayer = new ymaps.Layer(function(tile,zoom){
        return 'styles/img/mapedit.png';
    }, {
        tileTransparent: true
    });

    return {
        '.setting-button click':function(event){
            var settings = $(this.parentNode.querySelector('.settings'));
            if(settings.hasClass('hide'))
                settings.removeClass('hide');
            else{
                map.layers.remove(editLayer);
                settings.addClass('hide');
                edit = false;
                map.geoObjects.remove(newPoint);
                if(vectorPoint){
                    map.geoObjects.remove(vectorPoint);
                    vectorPoint = false;
                    map.geoObjects.remove(vectorLine);
                }
                if(polygon)
                    map.geoObjects.remove(polygon);
                var popup = event.target;
                while(!$(popup).hasClass('mcpopup')){
                    popup = popup.parentNode;
                }
                var editor = $(popup).find('.map-editor-add').addClass('hide');
                var editor = $(popup).find('.addtype').addClass('hide');
            }
        },
        '.add click':function(event){
            scope.$watch('noiseLevel',function(newV,oldV){
                if(newV !== oldV){
                    noise[0].value = newV.noiseLevel;
                    var n_msg = $(noise[0].parentNode.querySelector('.noise-level'));
                    if(newV.isPresent === true){
                        noise.removeClass('is-present-f')
                            .addClass('is-present-t');
                        n_msg.removeClass('is-present-f')
                            .addClass('is-present-t')
                    }else if(newV.isPresent === false){
                        noise.removeClass('is-present-t')
                            .addClass('is-present-f');
                        n_msg.removeClass('is-present-t')
                            .addClass('is-present-f');
                    }else{
                        noise.removeClass('is-present-t')
                            .removeClass('is-present-f');
                        n_msg.removeClass('is-present-t')
                            .removeClass('is-present-f');
                    }
                }
            });
            scope.$watch('speakers',function(newV,oldV){
                if(speakers){
                    console.log(speakers.hasClass('refresh-list'));
                    if(!speakers.hasClass('refresh-list'))
                        return false;
                    speakers.html('<option value="">громкоговорители</option>');
                    if(newV.length > 0){
                        for(var i = 0;i<newV.length;i++){
                            speakers.append('<option value="'+i+'">'+newV[i].hornSpeaker.name+'</option>');
                        }
                    }
                }
            });
            scope.$watch('fullSpeakersList',function(newV,oldV){
                if(newV !== oldV){
                    speakers.html('<option value="">громкоговорители</option>');
                    if(newV.content.length > 0){
                        for(var i = 0;i<newV.content.length;i++){
                            speakers.append('<option value="'+i+'">'+newV.content[i].name+'</option>');
                        }
                    }
                }
            });
            scope.$watch('calculatedZone',function(newV,oldV){
                if(newV !== oldV){
                    if(polygon){
                        map.geoObjects.remove(polygon);
                        polygon = false;
                    }
                    var geometry = {
                        geometry:{
                            type:"Polygon",
                            coordinates: [newV.coordinates,[]]
                        },
                        properties:{
                            balloonContent: newV.hornSpeaker.name
                        }
                    };
                    polygon = new ymaps.GeoObject(geometry,polygonOptions);
                    map.geoObjects.add(polygon);
                }
            });
            scope.$watch('authorized',function(nVal,oVal){
                //if(nVal !== oVal){
                    if(nVal === false){
                        scope.redirect();
                    }
                //}
            });
            var popup = event.target;
            while(!$(popup).hasClass('mcpopup')){
                popup = popup.parentNode;
            }
            var editors = $(popup).find('.map-editor-add');
            if(editors){
                editor = $(editors.find('table')[0]);
                centerx = $(editor).find('.centerx');
                centery = $(editor).find('.centery');
                vectorx = $(editor).find('.vectorx');
                vectory = $(editor).find('.vectory');
                vtype = $(editor).find('.vtype');
                height = $(editor).find('.height');
                noise = $(editor).find('.noise');
                speakers = $(editor).find('.speakers');
                editor.find('.fa-lock')
                    .addClass('fa-unlock')
                    .removeClass('fa-lock');
                centerx[0].value = '';
                centery[0].value = '';
                vectorx[0].value = '';
                vectory[0].value = '';
                height[0].value = '';
                noise[0].value = '';
                scope.speakers = [];
                //speakers.html('<option value="">громкоговорители</option>');
                lock = false;
                vlock = false;
                if(polygon)
                    map.geoObjects.remove(polygon);
            }
            if(!edit){
                editors.removeClass('hide');
                $(popup).find('.addtype').removeClass('hide');
                var coords = map.getCenter();
                centerx[0].value = coords[0];
                centery[0].value = coords[1];
                newPoint = new ymaps.GeoObject({
                        geometry: {
                            type: "Point",
                            coordinates: coords
                        },
                        properties: {
                            balloonContent: 'Новая точка'
                        }
                    },{
                        draggable:true,
                        preset: 'islands#circleIcon',
                        iconColor: '#00FF45'
                });
                newPoint.events.add('drag',function(event){
                    if(!lock){
                        var point = event.get('target');
                        var coords = point.geometry.getCoordinates();
                        centerx[0].value = coords[0];
                        centery[0].value = coords[1];
                    }
                });
                map.geoObjects.add(newPoint);
                map.layers.add(editLayer);
            }else{
                editors.addClass('hide');
                $(popup).find('.addtype').addClass('hide');
                map.layers.remove(editLayer);
                map.geoObjects.remove(newPoint);
                newPoint = false;
                if(vectorPoint){
                    map.geoObjects.remove(vectorPoint);
                    vectorPoint = false;
                    map.geoObjects.remove(vectorLine);
                }
            }
            edit = !edit;
        },
        '.lock click':function(event){
            var popup = event.target;
            while(!$(popup).hasClass('mcpopup')){
                popup = popup.parentNode;
            }
            var object = $(event.target);
            if(object.hasClass('fa-lock')){
                object.addClass('fa-unlock')
                    .removeClass('fa-lock');
                lock = false;
                scope.speakers = [];
                //speakers.html('<option value="">громкоговорители</option>');
                if(polygon)
                    map.geoObjects.remove(polygon);
                vectorx[0].value = '';
                vectory[0].value = '';
                if(vlock){
                    vlock = false;
                    $(popup).find('.vlock')
                        .addClass('fa-unlock')
                        .removeClass('fa-lock');
                    $(popup).find('.is-present-f')
                        .removeClass('is-present-f');
                    $(popup).find('.is-present-t')
                        .removeClass('is-present-t');
                }
                newPoint.options.set('draggable',true);
                map.geoObjects.remove(vectorPoint);
                map.geoObjects.remove(vectorLine);
            }else{
                object.addClass('fa-lock')
                    .removeClass('fa-unlock');
                lock = true;
                newPoint.options.set('draggable',false);
                vectorx[0].value = centerx[0].value;
                vectory[0].value = centery[0].value;
                vectorPoint = new ymaps.GeoObject({
                    geometry: {
                        type: "Point",
                        coordinates: [centerx[0].value,centery[0].value]
                    },
                    properties: {
                        balloonContent: 'Направление'
                    }
                },{
                    draggable:true,
                    preset: 'islands#circleIcon',
                    iconColor: '#0044FF'
                });
                var start = newPoint.geometry.getCoordinates();
                vectorLine.geometry.setCoordinates([start,start]);
                map.geoObjects.add(vectorLine);
                vectorPoint.events.add('drag',function(event){
                    if(!vlock){
                        var point = event.get('target');
                        var coords = point.geometry.getCoordinates();
                        vectorx[0].value = coords[0];
                        vectory[0].value = coords[1];
                        vectorLine.geometry.setCoordinates([start,coords]);
                    }
                });
                map.geoObjects.add(vectorPoint);
            }
        },
        '.vlock click':function(event){
            var object = $(event.target);
            if(!lock)
                return false;
            if(object.hasClass('fa-lock')){
                object.addClass('fa-unlock')
                    .removeClass('fa-lock');
                vlock = false;
                scope.speakers = [];
                //speakers.html('<option value="">громкоговорители</option>');
                if(polygon)
                    map.geoObjects.remove(polygon);
                vectorPoint.options.set('draggable',true);
            }else{
                if(newPoint.geometry.getCoordinates().join() === vectorPoint.geometry.getCoordinates().join()){
                    alert('Точка направления должна отличаться от точки установки');
                    return false;
                }else{
                    object.addClass('fa-lock')
                        .removeClass('fa-unlock');
                    vlock = true;
                    vectorPoint.options.set('draggable',false);
                    scope.getNoiseLevel({speakerLocation:newPoint.geometry.getCoordinates().join(','),destinationLocation:vectorPoint.geometry.getCoordinates().join(',')});
                }
            }
        },
        '.nlevel click':function(event){
            if(newPoint.geometry.getCoordinates().join() === vectorPoint.geometry.getCoordinates().join()){
                alert('Точка направления должна отличаться от точки установки');
                return false;
            }else{
                scope.getNoiseLevel({speakerLocation:newPoint.geometry.getCoordinates().join(','),destinationLocation:vectorPoint.geometry.getCoordinates().join(',')});
            }
        },
        '.noise click':function(event){
            var n_msg = $(noise[0].parentNode.querySelector('.noise-level'));
            noise.removeClass('is-present-f')
                .removeClass('is-present-t');
            n_msg.removeClass('is-present-f')
                .removeClass('is-present-t');
        },
        '.height keydown':function(event){
            var self = this;
            setTimeout(function(){
                if(self.value != h){
                    h = self.value;
                    //speakers.html('<option value="">громкоговорители</option>');
                }
            },100);
        },
        '.noise keydown':function(event){
            var self = this;
            setTimeout(function(){
                if(self.value != n){
                    n = self.value;
                    //speakers.html('<option value="">громкоговорители</option>');
                }
            },100);
        },
        '.speakers-list click':function(event){
            var object = $(event.target);
            if(!lock || !vlock){
                alert('Зафиксируйте точки.');
                return false;
            }
            if(!height[0].value || !noise[0].value || height[0].value == '' || noise[0].value == ''){
                alert('Заполните все поля формы.');
                return false;
            }
            if(polygon)
                map.geoObjects.remove(polygon);
            var s = {
                speakerLocation:newPoint.geometry.getCoordinates().join(','),
                mountHeight:height[0].value,
                noiseLevel:noise[0].value
            };
            s[vtype[0].value] = vectorPoint.geometry.getCoordinates().join(',');
            console.log(s)
            scope.getSpeakersList(s);
        },
        '.refresh-list change':function(event){
            if(polygon){
                map.geoObjects.remove(polygon);
                polygon = false;
            }
            if(scope.speakers && scope.speakers[this.value]){
                var p = scope.speakers[this.value];
                var geometry = {
                    geometry:{
                        type:"Polygon",
                        coordinates: [p.coordinates,[]]
                    },
                    properties:{
                        balloonContent: p.hornSpeaker.name
                    }
                };
                polygon = new ymaps.GeoObject(geometry,polygonOptions);
                map.geoObjects.add(polygon);
            }
        },
        '.full-list change':function(event){
            if(polygon){
                map.geoObjects.remove(polygon);
                polygon = false;
            }
        },
        '.save click':function(event){
            var coords = newPoint.geometry.getCoordinates();
            var vcoords = vectorPoint.geometry.getCoordinates();
            var myGeocoder = ymaps.geocode(coords,{results: 1});

            myGeocoder.then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);
                console.log(firstGeoObject);
                var object = {
                    speakerLocation:{latitude:coords[0].toFixed(6),longitude:coords[1].toFixed(6)},
                //    destinationLocation:{latitude:vcoords[0].toFixed(6),longitude:vcoords[1].toFixed(6)},
                    mountHeight:parseFloat(height[0].value),
                    noiseLevel:parseFloat(noise[0].value),
                  //  hornSpeaker:{id:parseFloat(scope.speakers[speakers[0].value].hornSpeaker.id)},
                    address:firstGeoObject.properties.get('text')
                };
                object[vtype[0].value] = {latitude:vcoords[0].toFixed(6),longitude:vcoords[1].toFixed(6)};
                if(scope.speakers.length == 0){
                    object.hornSpeaker = {id:scope.fullSpeakersList.content[speakers[0].value].id};
                }else{
                    object.hornSpeaker = {id:scope.speakers[speakers[0].value].hornSpeaker.id};
                }
                scope.createNewSPLZone(object,function(){
                    editor.find('.fa-lock')
                        .addClass('fa-unlock')
                        .removeClass('fa-lock');
                    editor.find('.is-present-f')
                        .removeClass('is-present-f');
                    editor.find('.is-present-t')
                        .removeClass('is-present-t');
                    vectorx[0].value = '';
                    vectory[0].value = '';
                    height[0].value = '';
                    noise[0].value = '';
                    speakers[0].value = '';
                    scope.speakers = [];
                    //speakers.html('<option value="">громкоговорители</option>');
                    lock = false;
                    vlock = false;
                    newPoint.options.set('draggable',true);
                    map.geoObjects.remove(vectorPoint);
                    map.geoObjects.remove(vectorLine);
                    if(polygon)
                        map.geoObjects.remove(polygon);
                });
            });
        },
        '.type click':function(event){
            var popup = event.target;
            while(!$(popup).hasClass('mcpopup')){
                popup = popup.parentNode;
            }
            var object = this.parentNode;
            $(object.parentNode.parentNode).find('.active')
                .removeClass('active');
            $(this.parentNode).addClass('active');
            var editors = $(popup).find('.map-editor-add table');
            var buttons = $(popup).find('.type');
            for(var i = 0;i<buttons.length;i++){
                if(!$(editors[i]).hasClass('hide')){
                    $(editors[i]).addClass('hide')
                }
                if(this == buttons[i]){
                    editor = $(editors[i]);
                    if($(editors[i]).hasClass('hide')){
                        $(editors[i]).removeClass('hide');
                        centerx = $(editors[i]).find('.centerx');
                        centery = $(editors[i]).find('.centery');
                        vectorx = $(editors[i]).find('.vectorx');
                        vectory = $(editors[i]).find('.vectory');
                        vtype = $(editors[i]).find('.vtype');
                        height = $(editors[i]).find('.height');
                        noise = $(editors[i]).find('.noise');
                        speakers = $(editors[i]).find('.speakers');
                        //speakers.html('<option value="">громкоговорители</option>');
                        if($(buttons[i]).hasClass('full-list')){
                            scope.getSpeakers();
                        }
                        else{
                            scope.speakers = [];
                            speakers.html('<option value="">громкоговорители</option>');
                        }
                        $(editors[i]).find('.fa-lock')
                            .addClass('fa-unlock')
                            .removeClass('fa-lock');
                        var coords = map.getCenter();
                        centerx[0].value = coords[0];
                        centery[0].value = coords[1];
                        vectorx[0].value = '';
                        vectory[0].value = '';
                        height[0].value = '';
                        noise[0].value = '';
                        scope.noiseLevel = {
                            noiseLevel:''
                        };
                        var n_msg = $(editor[0].querySelector('.noise-level'));
                        noise.removeClass('is-present-f')
                            .removeClass('is-present-f');
                        n_msg.removeClass('is-present-f')
                            .removeClass('is-present-f');
                        if(polygon)
                            map.geoObjects.remove(polygon);
                        if(vectorPoint){
                            map.geoObjects.remove(vectorPoint);
                            vectorPoint = false;
                            map.geoObjects.remove(vectorLine);
                        }
                        newPoint.options.set('draggable',true);
                        lock = false;
                        vlock = false;
                    }
                }
            }
            return false;
        },
        '.show-zone click':function(event){
            if(!lock || !vlock){
                alert('Зафиксируйте точки.');
                return false;
            }
            if(!height[0].value || !noise[0].value || !speakers[0].value || height[0].value == '' || noise[0].value == '' || speakers[0].value == ''){
                alert('Заполните все поля формы.');
                return false;
            }
            console.log(scope.speakers,speakers[0].value,scope.fullSpeakersList);

            var object = {
                speakerLocation:newPoint.geometry.getCoordinates().join(','),
                mountHeight:height[0].value,
                noiseLevel:noise[0].value,
                hornSpeaker:parseFloat(scope.fullSpeakersList.content[speakers[0].value].id)
            };
            object[vtype[0].value] = vectorPoint.geometry.getCoordinates().join(',');
            console.log(object);
            scope.getSpeakerZone(object);
        },
        '.cancel click':function(event){
            editor.find('.fa-lock')
                .addClass('fa-unlock')
                .removeClass('fa-lock');
            editor.find('.is-present-f')
                .removeClass('is-present-f');
            editor.find('.is-present-t')
                .removeClass('is-present-t');
            vectorx[0].value = '';
            vectory[0].value = '';
            height[0].value = '';
            noise[0].value = '';
            speakers[0].value = '';
            scope.speakers = [];
            //speakers.html('<option value="">громкоговорители</option>');
            lock = false;
            vlock = false;
            newPoint.options.set('draggable',true);
            map.geoObjects.remove(vectorPoint);
            map.geoObjects.remove(vectorLine);
            if(polygon)
                map.geoObjects.remove(polygon);
        }
    };
};