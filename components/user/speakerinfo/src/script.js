/**
 * Created by McPander on 29.11.2014.
 */
var events = function(scope,data){
    return {
        '.spl-info click':function(event){
            console.log('super',scope.about.length,data);
        },
        '.button-close click':function(event){
            console.log(scope.about,data);
            scope.removeSPLZInfo(data);
            stopEvent(event);
        },
        '.send-error click':function(event){
            var errorBlock = event.target.parentNode.querySelector('.error-block');
            if($(errorBlock).hasClass('display')){
                $(errorBlock).removeClass('display')
            }else{
                $(errorBlock).addClass('display')
            }
            stopEvent(event);
        },
        '.send click':function(event){
            var errorBlock = event.target.parentNode;
            var textarea = errorBlock.querySelector('textarea');
            var errorObject = {
                speakerLocation:"/repository/speakerLocation/"+data.speakerLocationId,
                comment:textarea.value
            };
            scope.sendSPLZError(errorObject);
            stopEvent(event);
        }
    };
};
function stopEvent(event){
    event.stopPropagation();
    event.cancelBubble = false;
}