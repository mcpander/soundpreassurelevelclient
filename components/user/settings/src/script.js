/**
 * Created by McPander on 29.11.2014.
 */

var events = function(scope,data){
    return {
        '.setting-button click':function(event){
            var settings = $(this.parentNode.querySelector('.settings'));
            if(settings.hasClass('hide'))
                settings.removeClass('hide');
            else
                settings.addClass('hide');
        },
        '.test change':function(event){
            var el = this;
            while(!$(el).hasClass('mcpopup')){
                el = el.parentNode;
            }
            el.querySelector('.quantity').disabled = !this.checked;
            scope.setTest(this.checked);
        },
        '.quantity keyup':function(event){
            var value = this.value;
            setTimeout(function(){
                scope.setLimit(value);
            },100)
        }
    };
};